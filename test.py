#!/usr/bin/env python
# -*- coding: utf-8 vi:noet

import logging
import os
import ctypes
import ctypes.util

from hypothesis import strategies as st
from hypothesis import given


logger = logging.getLogger(__name__)


dll = ctypes.CDLL(os.path.join(os.path.dirname(__file__), "libbsdiff.so"))
libc = ctypes.CDLL(ctypes.util.find_library("c"))


def make_patch(a, b):
	a_size = ctypes.c_size_t(len(a))
	b_size = ctypes.c_size_t(len(b))

	diff_buf = ctypes.c_voidp()
	diff_size = ctypes.c_size_t()

	res = dll.bsdiff_diff(
	 libc.malloc,
	 libc.free,
	 a, a_size,
	 b, b_size,
	 ctypes.byref(diff_buf), ctypes.byref(diff_size),
	)

	if res != 0:
		raise RuntimeError

	data = ctypes.string_at(diff_buf, diff_size.value)

	libc.free(diff_buf)

	return data


def apply_patch(a, b):
	#logger.info("%s %s", a, b)
	a_size = ctypes.c_size_t(len(a))
	b_size = ctypes.c_size_t(len(b))

	c_buf = ctypes.c_voidp()
	c_size = ctypes.c_size_t()

	res = dll.bsdiff_patch(
	 libc.malloc,
	 a, a_size,
	 b, b_size,
	 ctypes.byref(c_buf), ctypes.byref(c_size),
	)

	data = ctypes.string_at(c_buf, c_size.value)

	libc.free(c_buf)

	return data


@given(old=st.binary(max_size=400), new=st.binary(max_size=400))
def test_loopback(old, new):
	logger.debug("Patching back and forth “%s” -> “%s”", old.hex(), new.hex())
	patch = make_patch(old, new)
	logger.debug("Patch = “%s”", patch.hex())
	new_mes = apply_patch(old, patch)
	assert new_mes == new
