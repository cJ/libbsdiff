#########
libbsdiff
#########

libbsdiff is a libification / adaptation of the existing binary patch tool
`bsdiff <https://www.daemonology.net/bsdiff>`_.


Context
#######

The adaptation was done because the algorithm in bsdiff is small (diff
is ~ 2.7 kB of x86 code, patch is ~ 400 B) and other existing delta
compression libraries (eg. xdelta) are larger.

The adaptation was done considering the following goals:

- Have a C function call interface.

- Generate/apply patches without accessing files, without stdio.h.

  An external memory allocator/deallocator must be provided,
  which allows the code to run pretty much everywhere.

- Have a slightly more compact `patch data format`_.

  All the recorded offsets are encoded as variable-length integers,
  instead of being 8-byte. This reduces the header size, and the portion
  of the patch called control block.
  No magic header is added. The header can become as small as 3 bytes,
  instead of 32.

  The patch data is not compressed as in the original bsdiff (where
  control block, delta block, and extra block are each compressed
  separately).
  The user should do the compression; it is not complicated and gives
  greater control (on which compression to use, if any).

Meanwhile, the diffing algorithm was unmodified.


Performance
###########

.. admonition:: Note

   This section should be improved.

bsdiff is quite memory-hungry.
The algorithm requires max(17*n,9*n+m)+O(1) bytes of memory, where n is the size
of the old file and m is the size of the new file.
The implementation over-allocates.

bspatch requires n+m+O(1) bytes.

bsdiff runs in O((n+m) log n) time; on a 200MHz Pentium Pro, building
a binary patch for a 4MB file takes about 90 seconds.

bspatch runs in O(n+m) time; on the same machine, applying that patch takes about two seconds.


Patch Data Format
#################


The patch data contains:

- “control data“ size: varint
- “difference data“ size: varint
- “new file size”: varint
- “control data” containing 3-tuple of offsets (varint)

  The control data tuples are:

  - number of bytes that must undergo byte-wise addition (new = old + diff)
  - number of bytes to be copied from the extra block
  - number of bytes to seek forwards in the source data

- “difference”: each byte is the difference between a new byte and an old byte
- “new-only”: each byte is a byte to append

This can be compared to Vcdiff data format :rfc:`3284`.


Algorithm
#########

The algorithm used by BSDiff 4 uses “add and insert” and is described
in the paper `Naive differences of executable code <https://www.daemonology.net/papers/bsdiff.pdf>`_
please cite this in papers as:

   Colin Percival, Naive differences of executable code, http://www.daemonology.net/bsdiff/, 2003.


License
#######

BSD-3, as the original bsdiff.


TODO
####

- Better document the algorithm.
- Uniformize the coding style.
