/*-
 * Copyright 2022 Jérôme Carretero
 * Copyright 2003-2005 Colin Percival
 * All rights reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted providing that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "bsdiff.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MIN(x,y) (((x)<(y)) ? (x) : (y))

//#define debug(...) fprintf(stderr, __VA_ARGS__)
#define debug(...)

/*
*/
static void split(off_t *I,off_t *V,off_t start,off_t len,off_t h)
{
	off_t i,j,k,x,tmp,jj,kk;

	if(len<16) {
		for(k=start;k<start+len;k+=j) {
			j=1;x=V[I[k]+h];
			for(i=1;k+i<start+len;i++) {
				if(V[I[k+i]+h]<x) {
					x=V[I[k+i]+h];
					j=0;
				};
				if(V[I[k+i]+h]==x) {
					tmp=I[k+j];I[k+j]=I[k+i];I[k+i]=tmp;
					j++;
				};
			};
			for(i=0;i<j;i++) V[I[k+i]]=k+j-1;
			if(j==1) I[k]=-1;
		};
		return;
	};

	x=V[I[start+len/2]+h];
	jj=0;kk=0;
	for(i=start;i<start+len;i++) {
		if(V[I[i]+h]<x) jj++;
		if(V[I[i]+h]==x) kk++;
	};
	jj+=start;kk+=jj;

	i=start;j=0;k=0;
	while(i<jj) {
		if(V[I[i]+h]<x) {
			i++;
		} else if(V[I[i]+h]==x) {
			tmp=I[i];I[i]=I[jj+j];I[jj+j]=tmp;
			j++;
		} else {
			tmp=I[i];I[i]=I[kk+k];I[kk+k]=tmp;
			k++;
		};
	};

	while(jj+j<kk) {
		if(V[I[jj+j]+h]==x) {
			j++;
		} else {
			tmp=I[jj+j];I[jj+j]=I[kk+k];I[kk+k]=tmp;
			k++;
		};
	};

	if(jj>start) split(I,V,start,jj-start,h);

	for(i=0;i<kk-jj;i++) V[I[jj+i]]=kk-1;
	if(jj==kk-1) I[jj]=-1;

	if(start+len>kk) split(I,V,kk,start+len-kk,h);
}

/*!
*/
static void qsufsort(off_t *I,off_t *V,u_char *old,off_t oldsize)
{
	off_t buckets[256];
	off_t i,h,len;

	for(i=0;i<256;i++) buckets[i]=0;
	for(i=0;i<oldsize;i++) buckets[old[i]]++;
	for(i=1;i<256;i++) buckets[i]+=buckets[i-1];
	for(i=255;i>0;i--) buckets[i]=buckets[i-1];
	buckets[0]=0;

	for(i=0;i<oldsize;i++) I[++buckets[old[i]]]=i;
	I[0]=oldsize;
	for(i=0;i<oldsize;i++) V[i]=buckets[old[i]];
	V[oldsize]=0;
	for(i=1;i<256;i++) if(buckets[i]==buckets[i-1]+1) I[buckets[i]]=-1;
	I[0]=-1;

	for(h=1;I[0]!=-(oldsize+1);h+=h) {
		len=0;
		for(i=0;i<oldsize+1;) {
			if(I[i]<0) {
				len-=I[i];
				i-=I[i];
			} else {
				if(len) I[i-len]=-len;
				len=V[I[i]]+1-i;
				split(I,V,i,len,h);
				i+=len;
				len=0;
			};
		};
		if(len) I[i-len]=-len;
	};

	for(i=0;i<oldsize+1;i++) I[V[i]]=i;
}

/*! Compute length of matching prefix
*/
static off_t matchlen(u_char *old,off_t oldsize,u_char *new,off_t newsize)
{
	off_t i;

	for(i=0;(i<oldsize)&&(i<newsize);i++)
		if(old[i]!=new[i]) break;

	return i;
}

/*!
*/
static off_t search(off_t *I,u_char *old,off_t oldsize,
		u_char *new,off_t newsize,off_t st,off_t en,off_t *pos)
{
	off_t x,y;

	if(en-st<2) {
		x=matchlen(old+I[st],oldsize-I[st],new,newsize);
		y=matchlen(old+I[en],oldsize-I[en],new,newsize);

		if(x>y) {
			*pos=I[st];
			return x;
		} else {
			*pos=I[en];
			return y;
		}
	};

	x=st+(en-st)/2;
	if(memcmp(old+I[x],new,MIN(oldsize-I[x],newsize))<0) {
		return search(I,old,oldsize,new,newsize,x,en,pos);
	} else {
		return search(I,old,oldsize,new,newsize,st,x,pos);
	};
}

/*
  Output an offset
 */
static void offtout(off_t x, u_char *buf, u_char ** outpos)
{
	off_t zig = (x >= 0) ? 2*x : -2*x - 1;
	//debug("x=%zd zig=%zu\n", x, zig);
	size_t len = 0;

	do {
		unsigned char chunk = zig & 0x7f;
		if (zig == chunk) {
			//debug("chunk=%02x\n", chunk);
			buf[len++] = chunk;
		}
		else {
			//debug("chunk=%02x\n", chunk);
			buf[len++] = 0x80 | chunk;
		}
		zig >>= 7;
	} while (zig);

	*outpos = &buf[len];
}


int bsdiff_diff(
 malloc_t malloc,
 free_t free,
 u_char * old, off_t oldsize,
 u_char * new, off_t newsize,
 u_char ** out_buf, off_t * out_size)
{
	off_t *I,*V;
	off_t scan,pos,len;
	off_t lastscan,lastpos,lastoffset;
	off_t oldscore,scsc;
	off_t s,Sf,lenf,Sb,lenb;
	off_t overlap,Ss,lens;
	off_t i;

	u_char *cb, *db,*eb;
	off_t cblen, dblen, eblen;
	off_t res;

	off_t patch_size;
	u_char * patch; // temporary buffer for patch data

	u_char * patch_cur;
	u_char * cb_cur;

	debug("old %p %zu new %p %zu\n", old, oldsize, new, newsize);

	if (
		((I=malloc((oldsize+1)*sizeof(off_t)))==NULL) ||
		((V=malloc((oldsize+1)*sizeof(off_t)))==NULL)) {
		return __LINE__;
	}

	qsufsort(I, V, old, oldsize);

#if 1
	free(V);
#endif

	// TODO reduce over-allocation
	size_t cb_resv = (newsize) * sizeof(off_t) * 3;
	size_t db_resv = newsize;
	size_t eb_resv = newsize;

	if ((cb=malloc(cb_resv))==NULL) {
		return __LINE__;
	}

	if ((db=malloc(db_resv))==NULL) {
		return __LINE__;
	}

	if ((eb=malloc(eb_resv))==NULL) {
		return __LINE__;
	}

	dblen = 0;
	eblen = 0;

	size_t patch_resv = oldsize + newsize * 3 + 24;

	// TODO reduce over-allocation
	patch_resv = 0;
	patch_resv += newsize; // max size of db + eb is newsize
	patch_resv += sizeof(off_t)*3; // max size of header
	patch_resv += cb_resv; // max size of cb
	debug("Patch reserved size: %zu\n",
	 patch_resv);
	patch = malloc(patch_resv);

	cb_cur = cb;

	scan=0;len=0;
	lastscan=0;lastpos=0;lastoffset=0;
	while (scan<newsize) {
		oldscore=0;

		for (scsc=scan+=len;scan<newsize;scan++) {
			len = search(I,old,oldsize,new+scan,newsize-scan,
					0,oldsize,&pos);

			for(;scsc<scan+len;scsc++)
				if((scsc+lastoffset<oldsize) &&
				(old[scsc+lastoffset] == new[scsc]))
					oldscore++;

			if(((len==oldscore) && (len!=0)) ||
				(len>oldscore+8)) break;

			if((scan+lastoffset<oldsize) &&
				(old[scan+lastoffset] == new[scan]))
				oldscore--;
		};

		if((len!=oldscore) || (scan==newsize)) {
			s=0;Sf=0;lenf=0;
			for(i=0;(lastscan+i<scan)&&(lastpos+i<oldsize);) {
				if(old[lastpos+i]==new[lastscan+i]) s++;
				i++;
				if(s*2-i>Sf*2-lenf) { Sf=s; lenf=i; };
			};

			lenb=0;
			if(scan<newsize) {
				s=0;Sb=0;
				for(i=1;(scan>=lastscan+i)&&(pos>=i);i++) {
					if(old[pos-i]==new[scan-i]) s++;
					if(s*2-i>Sb*2-lenb) { Sb=s; lenb=i; };
				};
			};

			if(lastscan+lenf>scan-lenb) {
				overlap=(lastscan+lenf)-(scan-lenb);
				s=0;Ss=0;lens=0;
				for(i=0;i<overlap;i++) {
					if(new[lastscan+lenf-overlap+i]==
					   old[lastpos+lenf-overlap+i]) s++;
					if(new[scan-lenb+i]==
					   old[pos-lenb+i]) s--;
					if(s>Ss) { Ss=s; lens=i+1; };
				};

				lenf+=lens-overlap;
				lenb-=lens;
			};

			for(i=0;i<lenf;i++) {
				db[dblen+i] = new[lastscan+i]-old[lastpos+i];
			}

			size_t eb_chunk = (scan-lenb)-(lastscan+lenf);

			memcpy(&eb[eblen], &new[lastscan+lenf], eb_chunk);

			dblen += lenf;
			eblen += eb_chunk;
			ssize_t old_advance = (pos-lenb)-(lastpos+lenf);

			debug("Add len(diff) %zu\n", lenf);
			offtout(lenf, cb_cur, &cb_cur);

			debug("Add len(extra) %zu\n", (scan-lenb)-(lastscan+lenf));
			offtout(eb_chunk, cb_cur, &cb_cur);

			debug("Add old \x1B[33madvance\x1B[0m %zd\n", old_advance);
			offtout(old_advance, cb_cur, &cb_cur);

			if (cb_cur - cb > cb_resv) {
				debug("\x1B[31;1mcontrol data buffer overflow\x1B[0m\n");
				return __LINE__;
			}

			if (dblen > eb_resv) {
				debug("\x1B[31;1extra data buffer overflow\x1B[0m\n");
				return __LINE__;
			}

			if (eblen > eb_resv) {
				debug("\x1B[31;1extra data buffer overflow\x1B[0m\n");
				return __LINE__;
			}

			lastscan = scan-lenb;
			lastpos = pos-lenb;
			lastoffset = pos-scan;
		};
	};

	cblen = cb_cur - cb;

	debug("cblen=%zu\n", cblen);
	debug("dblen=%zu\n", dblen);
	debug("eblen=%zu\n", eblen);

	patch_cur = patch;

	offtout(cblen, patch_cur, &patch_cur);
	offtout(dblen, patch_cur, &patch_cur);
	offtout(newsize, patch_cur, &patch_cur);

	size_t header_len = patch_cur - patch;

	debug("Header length %zu\n",
	 header_len);

	memcpy(patch_cur, cb, cblen);
	patch_cur += cblen;

	memcpy(patch_cur, db, dblen);
	patch_cur += dblen;

	memcpy(patch_cur, eb, eblen);
	patch_cur += eblen;

	/* Free the memory we used */
	free(cb);
	free(db);
	free(eb);
	free(I);

	patch_size = patch_cur - patch;

	if (patch_size > patch_resv) {
		debug("\x1B[31;1mPatch size too large: %zu\x1B[0m\n",
		 patch_size);
		return __LINE__;
	}

	*out_buf = malloc(patch_size);
	memcpy(*out_buf, patch, patch_size);
	free(patch);
	*out_size = patch_size;

	return 0;
}

