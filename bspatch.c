/*
 * Copyright 2022 Jérôme Carretero
 * Copyright 2003-2005 Colin Percival
 * All rights reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted providing that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "bsdiff.h"
#include <stdio.h>
#include <string.h>

//#define debug(...) fprintf(stderr, __VA_ARGS__)
#define debug(...)

static off_t offtin(u_char *buf, u_char ** out)
{
	off_t zig = 0;
	int pos = 0;
	unsigned chunk;
	off_t ret;

	do {
		chunk = buf[pos];
		//debug("chunk = %02x pos=%d\n", chunk, pos);
		zig |= ((off_t)(chunk & 0x7f) << (pos*7));
		//debug("zig = %zd\n", zig);
		pos += 1;
	} while ((chunk & 0x80) != 0);

	//debug("zig = %zd\n", zig);

	if ((zig & 1) == 0) {
		ret = zig >> 1;
	}
	else {
		ret = - ((zig + 1) >> 1);
	}

	//debug("ret = %zd\n", ret);

	*out = &buf[pos];

	return ret;
}


int bsdiff_patch(malloc_t malloc,
 u_char * old, ssize_t oldsize,
 u_char * patch, ssize_t patchsize,
 u_char ** new_buf, ssize_t * new_size)
{
	ssize_t ctrllen,datalen;
	u_char * header;
	u_char buf[8];
	u_char *new;
	ssize_t newsize;
	off_t oldpos,newpos;
	off_t ctrl[3];
	off_t lenread;
	off_t i;
	u_char * cp, *dp, *ep;
	ssize_t header_size;

	header = patch;

	/* Read lengths from header */
	ctrllen = offtin(header, &header);
	datalen = offtin(header, &header);
	newsize = offtin(header, &header);

	debug("clen=%zu dlen=%zu size=%zu\n",
	 ctrllen, datalen, newsize);

	if (ctrllen<0) {
		return __LINE__;
	}
	if (datalen<0) {
		return __LINE__;
	}
	if (newsize<0) {
		return __LINE__;
	}

	header_size = header - patch;
	debug("header size %zu\n",
	 header_size);

	cp = &patch[header_size];
	dp = &patch[header_size+ctrllen];
	ep = &patch[header_size+ctrllen+datalen];

	if ((new = malloc(newsize)) == NULL) {
		return __LINE__;
	}

	oldpos=0;newpos=0;
	while (newpos<newsize) {
		/* Read control data */
		for(i=0;i<=2;i++) {
			ctrl[i]=offtin(cp, &cp);
		};

		/* Sanity-check */
		if(newpos+ctrl[0]>newsize) {
			return __LINE__;
		}

		/* Read diff string */
		memcpy(&new[newpos], dp, ctrl[0]);
		dp += ctrl[0];

		/* Add old data to diff string */
		for(i=0;i<ctrl[0];i++)
			if((oldpos+i>=0) && (oldpos+i<oldsize))
				new[newpos+i] += old[oldpos+i];

		/* Adjust pointers */
		newpos+=ctrl[0];
		oldpos+=ctrl[0];

		/* Sanity-check */
		if(newpos+ctrl[1]>newsize) {
			return __LINE__;
		}

		/* Read extra string */
		memcpy(&new[newpos], ep, ctrl[1]);
		ep += ctrl[1];

		/* Adjust pointers */
		newpos+=ctrl[1];
		oldpos+=ctrl[2];
	};

	*new_buf = new;
	*new_size = newsize;

	return 0;
}

