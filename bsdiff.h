#ifndef LIB_BSDIFF_H
#define LIB_BSDIFF_H

#include <stdlib.h>

/*! Memory allocation function

  malloc(0) must return a pointer that can be free()ed
*/
typedef void * malloc_t(size_t);


/*! Memory deallocation function
*/
typedef void free_t(void*);

#if defined(__cplusplus)
extern "C" {
#endif

/*!
  Generate (allocating) a binary patch between baseline buffer `old`,
  new buffer `new`.

  `out_buf` must be freed by the calling code, when unused.

  \param[in] malloc memory allocation function
  \param[in] free memory de-allocation function
  \param[in] old patch base buffer
  \param[in] oldsize patch base buffer size
  \param[in] new destination buffer
  \param[in] newsize destination buffer size
  \param[out] out_buf patch buffer
  \param[out] out_size patch buffer size
  \return 0 on success

 */
int bsdiff_diff(
 malloc_t malloc,
 free_t free,
 u_char * old, off_t oldsize,
 u_char * new, off_t newsize,
 u_char ** out_buf, off_t * out_size);

/*!
  Apply a binary patch `patch` on baseline buffer `old`,
  allocating a buffer for the new buffer.

  \param[in] malloc memory allocation function
  \param[in] old patch base buffer
  \param[in] oldsize patch base buffer size
  \param[in] patch patch buffer
  \param[in] patchsize patch buffer size
  \param[out] new_buf patched buffer
  \param[out] new_size patched buffer size
  \return 0 on success
 */
int bsdiff_patch(
 malloc_t malloc,
 u_char * old, ssize_t oldsize,
 u_char * patch, ssize_t patchsize,
 u_char ** new_buf, ssize_t * new_size);


#if defined(__cplusplus)
} // extern "C"
#endif

#endif
